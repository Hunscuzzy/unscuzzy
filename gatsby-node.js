/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it

const path = require(`path`);
const { slugify } = require("./src/utils/utilFunctions");
const _ = require("lodash");

const wrapper = (promise) =>
  promise.then((result) => {
    if (result.errors) {
      throw result.errors;
    }
    return result;
  });

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;

  const caseStudyPage = path.resolve(
    "./src/templates/case-study-template/case-study-template.js"
  );
  const legalsTemplatePage = path.resolve(
    "./src/templates/legals-template/legals-template.js"
  );
  const singleBlogPage = path.resolve(
    "./src/templates/blog-template/blog-template.js"
  );
  const blogList = path.resolve("./src/templates/blog-list/blog-list.js");
  const servicePage = path.resolve(
    "./src/templates/service-template/service-template.js"
  );

  const result = await wrapper(
    graphql(`
      {
        allPrismicPortfolioPost(sort: { order: DESC, fields: data___date }) {
          edges {
            node {
              uid
              id
            }
          }
        }
        allPrismicLegals {
          edges {
            node {
              uid
              id
            }
          }
        }
        allPrismicCategories {
          edges {
            node {
              uid
              id
            }
          }
        }
        allPrismicBlogPost(sort: { order: DESC, fields: data___date }) {
          edges {
            node {
              uid
              id
            }
            next {
              uid
              id
              data {
                title {
                  text
                }
                thumbnail {
                  localFile {
                    childImageSharp {
                      fluid(maxWidth: 720, maxHeight: 120, quality: 100) {
                        src
                        presentationWidth
                        presentationHeight
                      }
                    }
                  }
                }
              }
            }
            previous {
              uid
              id
              data {
                title {
                  text
                }
                thumbnail {
                  localFile {
                    childImageSharp {
                      fluid(maxWidth: 720, maxHeight: 120, quality: 100) {
                        src
                        presentationWidth
                        presentationHeight
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    `)
  );

  // Create Single IT Service page

  const itservices = result.data.allPrismicCategories.edges;
  itservices.forEach((edge) => {
    createPage({
      path: `services/${edge.node.uid}`,
      component: servicePage,
      context: {
        uid: edge.node.uid,
      },
    });
  });

  const legalPages = result.data.allPrismicLegals.edges;
  legalPages.forEach((edge) => {
    createPage({
      path: `/${edge.node.uid}`,
      component: legalsTemplatePage,
      context: {
        uid: edge.node.uid,
      },
    });
  });

  // Create Single Case Study Page
  const caseStudies = result.data.allPrismicPortfolioPost.edges;
  caseStudies.forEach((edge) => {
    createPage({
      path: `portfolio/${edge.node.uid}`,
      component: caseStudyPage,
      context: {
        uid: edge.node.uid,
      },
    });
  });

  // Create Single Blog Page
  const posts = result.data.allPrismicBlogPost.edges;
  posts.forEach((edge) => {
    createPage({
      path: `blog/${edge.node.uid}`,
      component: singleBlogPage,
      context: {
        uid: edge.node.uid,
        next: edge.next,
        previous: edge.previous,
      },
    });
  });

  // Create Blog List Page
  // Pagination
  const postsPerPage = 6;
  const numberOfPages = Math.ceil(posts.length / postsPerPage);

  Array.from({ length: numberOfPages }).forEach((_, index) => {
    const isFirstPage = index === 0;
    const currentPage = index + 1;
    if (isFirstPage) return;
    createPage({
      path: `blog/page-${currentPage}`,
      component: blogList,
      context: {
        limit: postsPerPage,
        skip: index * postsPerPage,
        currentPage,
        numberOfPages,
      },
    });
  });
};
