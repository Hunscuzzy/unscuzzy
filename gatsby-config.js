require("dotenv").config({
  path: `.env`,
});

const prismicHtmlSerializer = require("./src/gatsby/htmlSerializer");

module.exports = {
  pathPrefix: `/`,
  siteMetadata: {
    title: "Le sérieux d'une agence, la fléxibilité d'un freelance.",
    titleTemplate: `Unscuzzy`,
    description: `Unscuzzy conçoit des sites internet modernes, évolutifs et adaptables à tous les supports.`,
    author: `@unscuzzy`,
    image: "/landing.png",
    siteUrl: "https://unscuzzy.com",
    getform: "https://getform.io/f/4abfd7b5-736d-4c33-9057-538439f86ef6",
    copyright:
      "UNSCUZZY. Le sérieux d'une agence, la fléxibilité d'un freelance.",
    social: {
      facebook: "https://www.facebook.com/Unscuzzy/",
      instagram: "https://www.instagram.com/unscuzzy/",
      linkedin: "https://www.linkedin.com/company/9307368/admin/",
    },
    contact: {
      phone: "+33 666 81 95 07",
      address: "Pl. Músico Óscar Tordera Iñesta, 11, 03004 Alicante",
      email: "contact@unscuzzy.com",
      website: "https://unscuzzy.com/",
      rating: "4.9",
      customers: "47",
      clients: "3200",
      addressInfos: [
        {
          id: "alicante",
          state: "Alicante",
          address: "Pl. Músico Óscar Tordera Iñesta, 11, 03004 Alicante",
          email: "contact@unscuzzy.com",
          phone: "+33 666 81 95 07",
        },
      ],
    },
  },
  mapping: {
    "MarkdownRemark.frontmatter.author": `AuthorsJson.name`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-json`,
    `gatsby-plugin-playground`,
    `gatsby-plugin-styled-components`,
    "gatsby-plugin-loadable-components-ssr",
    {
      resolve: `gatsby-plugin-layout`,
      options: {
        component: require.resolve(`./src/containers/layout/layout.js`),
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        tableOfContents: {
          heading: null,
          maxDepth: 6,
        },
        excerpt_separator: `<!-- endexcerpt -->`,
      },
    },
    {
      resolve: `gatsby-remark-images`,
      options: {
        maxWidth: 1920,
        tracedSVG: { threshold: 128, color: "#880000" },
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `fonts`,
        path: `${__dirname}/src/assets/fonts`,
        ignore: [`**/\.*`],
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/assets/images`,
        ignore: [`**/\.*`],
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `data`,
        path: `${__dirname}/src/data`,
        ignore: [`**/\.*`],
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: "Mitech",
        short_name: "mitech",
        theme_color: "#086ad8",
        background_color: "#ffffff",
        display: "standalone",
        scope: "/",
        start_url: "/",
        icon: "src/assets/images/favicon.png",
        icons: [
          {
            src: "/icons/icon-72x72.png",
            sizes: "72x72",
            type: "image/png",
          },
          {
            src: "/icons/icon-96x96.png",
            sizes: "96x96",
            type: "image/png",
          },
          {
            src: "/icons/icon-128x128.png",
            sizes: "128x128",
            type: "image/png",
          },
          {
            src: "/icons/icon-144x144.png",
            sizes: "144x144",
            type: "image/png",
          },
          {
            src: "/icons/icon-152x152.png",
            sizes: "152x152",
            type: "image/png",
          },
          {
            src: "/icons/icon-192x192.png",
            sizes: "192x192",
            type: "image/png",
          },
          {
            src: "/icons/icon-384x384.png",
            sizes: "384x384",
            type: "image/png",
          },
          {
            src: "/icons/icon-512x512.png",
            sizes: "512x512",
            type: "image/png",
          },
        ],
      },
    },
    {
      resolve: "gatsby-source-prismic",
      options: {
        repositoryName: "unscuzzy",
        accessToken: `${process.env.API_KEY}`,
        schemas: {
          homepage: require("./src/schemas/homepage.json"),
          about: require("./src/schemas/about.json"),
          archive_blog: require("./src/schemas/archive_blog.json"),
          archive_categories: require("./src/schemas/archive_categories.json"),
          archive_portfolio: require("./src/schemas/archive_portfolio.json"),
          contact: require("./src/schemas/contact.json"),
          portfolio_post: require("./src/schemas/portfolio_post.json"),
          blog_post: require("./src/schemas/blog_post.json"),
          legals: require("./src/schemas/legals.json"),
          categories: require("./src/schemas/categories.json"),
        },
        shouldDownloadImage: ({ node, key, value }) => true,
      },
    },
    {
      resolve: `gatsby-plugin-breadcrumb`,
      options: {
        useAutoGen: true,
        autoGenHomeLabel: `Accueil`,
        exclude: [`/dev-404-page`, `/404`, `/404.html`],
        useClassNames: true,
      },
    },
    {
      resolve: "gatsby-plugin-robots-txt",
      options: {
        host: "https://unscuzzy.com/",
        sitemap: "https://unscuzzy.com/sitemap.xml",
        policy: [{ userAgent: "*", allow: "/" }],
      },
    },
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [`convergence`, `roboto\:300,400,700`],
        display: "swap",
      },
    },
    {
      resolve: `gatsby-plugin-offline`,
      options: {
        precachePages: [
          // `/about-us/`,
        ],
      },
    },
    // {
    //   resolve: `gatsby-plugin-sitemap`,
    //   options: {
    //     exclude: [`/blog/page-*`],
    //     query: `
    //       {
    //         allSitePage {
    //           edges {
    //             node {
    //               path
    //             }
    //           }
    //         }
    //         site {
    //           siteMetadata {
    //             siteUrl
    //           }
    //         }
    //     }`,
    //     resolveSiteUrl: ({ siteMetadata }) => {
    //       return siteMetadata.siteUrl;
    //     },
    //     serialize: ({ siteMetadata, allSitePage }) =>
    //       allSitePage.nodes.map((node) => {
    //         return {
    //           url: `${siteMetadata.siteUrl}${node.path}`,
    //           changefreq: `daily`,
    //           priority: 0.7,
    //         };
    //       }),
    //   },
    // },
  ],
};
