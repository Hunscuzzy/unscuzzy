import React from "react";
import PropTypes from "prop-types";
import Typed from "react-typed";
import "react-typed/dist/animatedCursor.css";

import SEO from "../components/seo";
import Layout from "../containers/layout/layout";
import Header from "../containers/layout/header";
import Footer from "../containers/layout/footer";
import PageHeader from "../components/pageheader";
import Section, { Row, Col, Box } from "../components/ui/wrapper";
import Sidebar from "../containers/blog/sidebar";
import BlogArea from "../containers/blog/blog-area";
import CTA from "../containers/global/cta-area/section-one";
import { TypedTextWrap } from "../containers/elements/typed-text/style";
import { StaticQuery } from "gatsby";

const BlogListLargeImage = ({ pageContext, location, ...restProps }) => {
  const { sectionStyle, headingStyle } = restProps;
  return (
    <StaticQuery
      query={graphql`
        query BlogArchivePageQuery {
          prismicArchiveBlog {
            data {
              keywords
              meta_description
              meta_title
              page_title
              cover_img {
                localFile {
                  childImageSharp {
                    fluid(maxWidth: 1920, maxHeight: 400, quality: 90) {
                      ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
              pub {
                link {
                  url
                }
                pub_image {
                  localFile {
                    childImageSharp {
                      fluid(maxWidth: 680, quality: 90) {
                        ...GatsbyImageSharpFluid_withWebp
                        presentationWidth
                        presentationHeight
                      }
                    }
                  }
                }
              }
            }
          }
        }
      `}
      render={(data) => (
        <Layout location={location}>
          <SEO
            title={data.prismicArchiveBlog.data.meta_title}
            description={data.prismicArchiveBlog.data.meta_description}
            keywords={data.prismicArchiveBlog.data.meta_keywords}
          />
          <Header />
          <PageHeader
            pageContext={pageContext}
            location={location}
            title={data.prismicArchiveBlog.data.page_title}
            image={
              data.prismicArchiveBlog.data.cover_img.localFile.childImageSharp
            }
          />
          <main className='site-wrapper-reveal'>
            <Section {...sectionStyle}>
              <Row>
                <Col lg={{ span: 4, order: 1 }} xs={{ span: 12, order: 2 }}>
                  <Sidebar pub={data.prismicArchiveBlog.data.pub} />
                </Col>
                <Col lg={{ span: 8, order: 2 }} xs={{ span: 12, order: 1 }}>
                  <Box mb='30px'>
                    <TypedTextWrap {...headingStyle}>
                      <span className='not-typical'>
                        Le blog qui parle de <br />
                      </span>
                      <Typed
                        strings={[
                          "gestion de projet",
                          "développement web",
                          "webdesign",
                          "référencement",
                        ]}
                        typeSpeed={50}
                        backSpeed={70}
                        loop
                      />
                    </TypedTextWrap>
                  </Box>
                  <BlogArea />
                </Col>
              </Row>
            </Section>
            <CTA />
          </main>
          <Footer />
        </Layout>
      )}
    />
  );
};

BlogListLargeImage.propTypes = {
  sectionStyle: PropTypes.object,
};

BlogListLargeImage.defaultProps = {
  sectionStyle: {
    pt: "89px",
    pb: "100px",
    responsive: {
      medium: {
        pt: "72px",
        pb: "80px",
      },
      small: {
        pt: "53px",
        pb: "60px",
      },
    },
  },
  headingStyle: {
    as: "h4",
    mb: "70px",
    textalign: "center",
    child: {
      color: "primary",
    },
    responsive: {
      medium: {
        mb: "50px",
      },
    },
  },
};

export default BlogListLargeImage;
