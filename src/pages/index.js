import React from "react";
import { StaticQuery, graphql } from "gatsby";

import SEO from "../components/seo";
import Layout from "../containers/layout/layout";
import Header from "../containers/layout/header";
import Footer from "../containers/layout/footer";
import HeroArea from "../containers/index-processing/hero-area";
import AboutArea from "../containers/index-processing/about-area";
import BlogArea from "../containers/blog/blog-preview";
import ContactArea from "../containers/global/contact-area/contact-three";
import CaseStudyArea from "../containers/index-infotechno/case-study-area";
import SliceZone from "../components/slicesZone";

const IndexProcessing = ({ location }) => (
  <StaticQuery
    query={graphql`
      query HpHeadingQuery {
        allPrismicPortfolioPost(
          limit: 3
          sort: { order: DESC, fields: data___date }
        ) {
          edges {
            node {
              uid
              data {
                meta_description
                title {
                  text
                }
                thumbnail {
                  localFile {
                    childImageSharp {
                      fluid(maxWidth: 480, maxHeight: 298, quality: 90) {
                        ...GatsbyImageSharpFluid_withWebp_tracedSVG
                        presentationWidth
                        presentationHeight
                      }
                    }
                  }
                }
              }
            }
          }
        }
        featureBlog: allPrismicBlogPost(
          limit: 1
          sort: { fields: data___date, order: DESC }
        ) {
          edges {
            node {
              uid
              data {
                date
                meta_description
                title {
                  text
                }
                thumbnail {
                  localFile {
                    childImageSharp {
                      fluid(maxWidth: 900, quality: 90) {
                        ...GatsbyImageSharpFluid_withWebp_tracedSVG
                        presentationWidth
                        presentationHeight
                      }
                    }
                  }
                }
              }
            }
          }
        }
        recentBlogs: allPrismicBlogPost(
          limit: 3
          skip: 1
          sort: { fields: data___date, order: DESC }
        ) {
          edges {
            node {
              uid
              data {
                date
                meta_description
                title {
                  text
                }
                thumbnail {
                  localFile {
                    childImageSharp {
                      fluid(maxWidth: 480, maxHeight: 298, quality: 90) {
                        ...GatsbyImageSharpFluid_withWebp_tracedSVG
                        presentationWidth
                        presentationHeight
                      }
                    }
                  }
                }
              }
            }
          }
        }
        prismicHomepage {
          id
          data {
            meta_description
            meta_title
            keywords
            body {
              ... on PrismicHomepageBodyClients {
                id
                items {
                  lien {
                    url
                    target
                  }
                  logo {
                    alt
                    localFile {
                      childImageSharp {
                        fixed(width: 120) {
                          ...GatsbyImageSharpFixed
                        }
                      }
                    }
                  }
                }
                slice_type
              }
              ... on PrismicHomepageBodyTemoignages {
                id
                slice_type
                items {
                  citation {
                    html
                  }
                  entreprise {
                    text
                  }
                  nom {
                    text
                  }
                  logo___photo {
                    alt
                    localFile {
                      childImageSharp {
                        fixed(width: 90, quality: 90) {
                          ...GatsbyImageSharpFixed
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    `}
    render={(data) => (
      <Layout location={location}>
        <SEO
          title={data.prismicHomepage.data.meta_title}
          description={data.prismicHomepage.data.meta_description}
          keywords={data.prismicHomepage.data.keywords}
        />
        <Header />
        <main className='site-wrapper-reveal'>
          <HeroArea />
          <AboutArea />
          <CaseStudyArea projects={data.allPrismicPortfolioPost.edges} />
          <BlogArea
            featureBlogs={data.featureBlog.edges}
            recentBlogs={data.recentBlogs.edges}
          />
          <SliceZone allSlices={data.prismicHomepage.data.body} />
          <ContactArea />
        </main>
        <Footer />
      </Layout>
    )}
  />
);

export default IndexProcessing;
