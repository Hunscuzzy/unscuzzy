import React from "react";
import SEO from "../components/seo";
import Layout from "../containers/layout/layout";
import Header from "../containers/layout/header";
import Footer from "../containers/layout/footer";
import PageHeader from "../components/pageheader";
import CaseStudyArea from "../containers/index-infotechno/case-study-area";
import CTAArea from "../containers/global/cta-area/section-one";
import { StaticQuery } from "gatsby";

const CaseStudiesPage = ({ pageContext, location }) => (
  <StaticQuery
    query={graphql`
      query Portfolio {
        prismicArchivePortfolio {
          data {
            cover_img {
              localFile {
                childImageSharp {
                  fluid(maxWidth: 1920, maxHeight: 400, quality: 90) {
                    ...GatsbyImageSharpFluid
                  }
                }
              }
            }
            keywords
            meta_description
            meta_title
            page_title
          }
        }
        allPrismicPortfolioPost(sort: { order: DESC, fields: data___date }) {
          edges {
            node {
              uid
              data {
                meta_description
                title {
                  text
                }
                thumbnail {
                  localFile {
                    childImageSharp {
                      fluid(maxWidth: 480, maxHeight: 298, quality: 90) {
                        ...GatsbyImageSharpFluid_withWebp_tracedSVG
                        presentationWidth
                        presentationHeight
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    `}
    render={(data) => (
      <Layout location={location}>
        <SEO
          title={data.prismicArchivePortfolio.data.meta_title}
          description={data.prismicArchivePortfolio.data.meta_description}
          keywords={data.prismicArchivePortfolio.data.meta_keywords}
        />
        <Header />
        <PageHeader
          pageContext={pageContext}
          location={location}
          title={data.prismicArchivePortfolio.data.page_title}
          image={
            data.prismicArchivePortfolio.data.cover_img.localFile
              .childImageSharp
          }
        />
        <main className='site-wrapper-reveal'>
          <CaseStudyArea projects={data.allPrismicPortfolioPost.edges} />
          <CTAArea />
        </main>
        <Footer />
      </Layout>
    )}
  />
);
export default CaseStudiesPage;
