import React from "react";
import SEO from "../components/seo";
import Layout from "../containers/layout/layout";
import Header from "../containers/layout/header";
import Footer from "../containers/layout/footer";
import PageHeader from "../components/pageheader";
import AboutDesc from "../containers/about-us/about-area";
import GradationArea from "../containers/elements/gradation";
import FaqSection from "../containers/faq/section-one";
import CTASection from "../containers/global/cta-area/section-one";
import { StaticQuery } from "gatsby";

const IndexProcessing = ({ pageContext, location }) => (
  <StaticQuery
    query={graphql`
      query AboutPageQuery {
        prismicAbout {
          data {
            meta_description
            meta_title
            page_title
            keywords
            cover_img {
              localFile {
                childImageSharp {
                  fluid(maxWidth: 1920, maxHeight: 400, quality: 90) {
                    ...GatsbyImageSharpFluid
                  }
                }
              }
            }
          }
        }
      }
    `}
    render={(data) => (
      <Layout location={location}>
        <SEO
          title={data.prismicAbout.data.meta_title}
          description={data.prismicAbout.data.meta_description}
          keywords={data.prismicAbout.data.keywords}
        />
        <Header />
        <main className='site-wrapper-reveal'>
          <PageHeader
            pageContext={pageContext}
            location={location}
            title={data.prismicAbout.data.page_title}
            image={data.prismicAbout.data.cover_img.localFile.childImageSharp}
          />
          <AboutDesc />
          <GradationArea />
          <CTASection />
          <FaqSection />
        </main>
        <Footer />
      </Layout>
    )}
  />
);

export default IndexProcessing;
