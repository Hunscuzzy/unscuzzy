import React from "react";
import SEO from "../components/seo";
import Layout from "../containers/layout/layout";
import Header from "../containers/layout/header";
import Footer from "../containers/layout/footer";
import PageHeader from "../components/pageheader";
import ServicesArea from "../containers/services";
import ContactArea from "../containers/global/contact-area/contact-three";
import { StaticQuery } from "gatsby";

const ServicesPage = ({ pageContext, location }) => (
  <StaticQuery
    query={graphql`
      query Services {
        prismicArchiveCategories {
          data {
            cover_img {
              localFile {
                childImageSharp {
                  fluid(maxWidth: 1920, maxHeight: 400, quality: 90) {
                    ...GatsbyImageSharpFluid
                  }
                }
              }
            }
            keywords
            meta_description
            meta_title
            page_title
            sous_titre {
              html
            }
          }
        }
      }
    `}
    render={(data) => (
      <Layout location={location}>
        <SEO
          title={data.prismicArchiveCategories.data.meta_title}
          description={data.prismicArchiveCategories.data.meta_description}
          keywords={data.prismicArchiveCategories.data.meta_keywords}
        />
        <Header />
        <PageHeader
          pageContext={pageContext}
          location={location}
          title={data.prismicArchiveCategories.data.page_title}
          image={
            data.prismicArchiveCategories.data.cover_img.localFile
              .childImageSharp
          }
        />
        <main className='site-wrapper-reveal'>
          <ServicesArea
            subtitle={data.prismicArchiveCategories.data.sous_titre.html}
          />
          <ContactArea />
        </main>
        <Footer />
      </Layout>
    )}
  />
);

export default ServicesPage;
