import React from "react";
import SEO from "../components/seo";
import Layout from "../containers/layout/layout";
import Header from "../containers/layout/header";
import Footer from "../containers/layout/footer";
import PageHeader from "../components/pageheader";
import CTAArea from "../containers/global/cta-area/section-one";
import ContactFormArea from "../containers/contact-us/contact-form-area";
import { StaticQuery } from "gatsby";
import Heading from "../components/ui/heading";
import { Col, Container, Row } from "../components/ui/wrapper";

const ContactUsPage = ({ pageContext, location }) => (
  <StaticQuery
    query={graphql`
      query ContactPageQuery {
        prismicContact {
          data {
            keywords
            meta_description
            meta_title
            subtitle {
              text
            }
            titre {
              text
            }
            bg_image {
              localFile {
                childImageSharp {
                  fluid(maxWidth: 1920, maxHeight: 400, quality: 90) {
                    ...GatsbyImageSharpFluid
                  }
                }
              }
            }
          }
        }
      }
    `}
    render={(data) => (
      <Layout location={location}>
        <SEO
          title={data.prismicContact.data.meta_title}
          description={data.prismicContact.data.meta_description}
          keywords={data.prismicContact.data.meta_keywords}
        />
        <Header />
        <PageHeader
          pageContext={pageContext}
          location={location}
          title={data.prismicContact.data.titre.text}
          image={data.prismicContact.data.bg_image.localFile.childImageSharp}
        />
        <main className='site-wrapper-reveal'>
          <Container pt='100px'>
            <Row>
              <Col lg={12} mb='50px' textalign='center'>
                <Heading>{data.prismicContact.data.subtitle.text}</Heading>
              </Col>
            </Row>
          </Container>
          <ContactFormArea titre={data.prismicContact.data.subtitle.text} />
          <CTAArea />
        </main>
        <Footer />
      </Layout>
    )}
  />
);

export default ContactUsPage;
