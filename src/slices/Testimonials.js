import React from "react";
import PropTypes from "prop-types";
import TestimonialArea from "../containers/global/testimonial-area/section-one";

const Testimonials = ({ input }) => {
  const testimonials = input.items;
  return <TestimonialArea testimonials={testimonials} />;
};

export default Testimonials;

Testimonials.propTypes = {
  input: PropTypes.object.isRequired,
};
