import React from "react";
import PropTypes from "prop-types";
import CodeBlock from "../components/code-block/code-block";
import { Col, Container, Row } from "../components/ui/wrapper";

const Code = ({ input }) => {
  const code = input.primary.code.text;
  const language = input.primary.language;
  console.log(language);
  return (
    <Container>
      <Row>
        <Col lg={10} ml='auto' mr='auto'>
          <CodeBlock
            code={code}
            language={language}
            plugins={["line-numbers"]}
          />
        </Col>
      </Row>
    </Container>
  );
};

export default Code;

Code.propTypes = {
  input: PropTypes.object.isRequired,
};
