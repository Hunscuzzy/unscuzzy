import React from "react";
import PropTypes from "prop-types";
import { Col, Container, Row } from "../components/ui/wrapper";

const Text = ({ input }) => {
  const text = input.primary.text.html;
  return (
    <Container>
      <Row>
        <Col lg={8} ml='auto' mr='auto' mb='50px'>
          <div dangerouslySetInnerHTML={{ __html: text }} />
        </Col>
      </Row>
    </Container>
  );
};

export default Text;

Text.propTypes = {
  input: PropTypes.object.isRequired,
};
