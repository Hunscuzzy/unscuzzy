import React from "react";
import PropTypes from "prop-types";
import ClientsArea from "../containers/global/clients-area";

const Clients = ({ input }) => {
  const clients = input.items;
  return <ClientsArea clients={clients} />;
};

export default Clients;

Clients.propTypes = {
  input: PropTypes.object.isRequired,
};
