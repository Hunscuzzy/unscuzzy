import Clients from "./Clients";
import Testimonials from "./Testimonials";
import Text from "./Text";
import ImgGallery from "./ImgGallery";
import Code from "./Code";

export { Clients, Testimonials, Text, ImgGallery, Code };
