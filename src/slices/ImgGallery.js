import React from "react";
import PropTypes from "prop-types";
import SmoothTransitionSlider from "../containers/elements/flexible-image-slider/smooth-transition-slider";

const ImgGallery = ({ input }) => {
  const images = input.items;
  return <SmoothTransitionSlider images={images} />;
};

export default ImgGallery;

ImgGallery.propTypes = {
  input: PropTypes.object.isRequired,
};
