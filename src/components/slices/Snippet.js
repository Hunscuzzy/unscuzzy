import React from "react";
import Box from "@material-ui/core/Box";
import Prism from "prismjs";

class PrismCode extends React.Component {
  constructor(props) {
    super(props);
    this.ref = React.createRef();
  }

  componentDidMount() {
    setTimeout(() => {
      this.highlight();
    }, 0);
  }

  componentDidUpdate() {
    setTimeout(() => {
      this.highlight();
    }, 0);
  }

  highlight = () => {
    if (this.ref && this.ref.current) {
      Prism.highlightElement(this.ref.current);
      Prism.highlightAll();
    }
  };

  render() {
    const { code, plugins, language } = this.props;
    return (
      <pre className={!plugins ? "" : plugins.join("")}>
        <code ref={this.ref} className={`language-${language}`}>
          {code.trim()}
        </code>
      </pre>
    );
  }
}

export default function Snippet({ slice }) {
  const { language, code } = slice.primary;
  return (
    <Box py={5}>
      <PrismCode
        code={code[0].text}
        language={language}
        plugins={["line-numbers"]}
      />
    </Box>
  );
}
