import React from "react";
import PropTypes from "prop-types";
import Img from "gatsby-image";
import {
  TestimonialWrap,
  TestimonialInfo,
  TestimonialMedia,
  TestimonialAuthor,
  AuthorInfo,
  AuthorName,
  AuthorRole,
  Review,
} from "./testimonial.style";

const Testimonial = ({
  authorName,
  authorRole,
  authorImg,
  review,
  ...testimonialStyle
}) => {
  const { wrapperStyle, infoStyle, imageStyle, reviewStyle } = testimonialStyle;
  let authorImage;
  if (authorImg && typeof authorImg.fixed !== "function") {
    authorImage = <Img fixed={authorImg.fixed} alt={authorName} />;
  } else {
    authorImage = (
      <img src={authorImg} className='img-fluid' alt={authorName} />
    );
  }

  return (
    <TestimonialWrap {...wrapperStyle}>
      <TestimonialInfo {...infoStyle}>
        {authorImg && (
          <TestimonialMedia {...imageStyle}>{authorImage}</TestimonialMedia>
        )}
        <TestimonialAuthor>
          <AuthorInfo>
            {authorName && <AuthorName>{authorName}</AuthorName>}
            {authorRole && <AuthorRole>{authorRole}</AuthorRole>}
          </AuthorInfo>
        </TestimonialAuthor>
      </TestimonialInfo>
      {review && (
        <Review {...reviewStyle} dangerouslySetInnerHTML={{ __html: review }} />
      )}
    </TestimonialWrap>
  );
};

Testimonial.propTypes = {
  authorImg: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  rating: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  authorName: PropTypes.string,
  authroRole: PropTypes.string,
  review: PropTypes.string,
  testimonialStyle: PropTypes.object,
};

Testimonial.defaultProps = {
  imageStyle: {
    circle: true,
  },
};

export default Testimonial;
