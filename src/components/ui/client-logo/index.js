import React from "react";
import PropTypes from "prop-types";
import Img from "gatsby-image";
import Image from "../../image";
import { BrandLogoWrap } from "./client-logo.style";

const ClientLogo = ({ path, brandImage, title, ...props }) => {
  let brandImg;
  if (brandImage.fixed) {
    brandImg = <Img fixed={brandImage.fixed} alt={title} />;
  } else if (brandImage.fluid) {
    brandImg = <Image fluid={brandImage.fluid} alt={title} />;
  } else {
    brandImg = <img src={brandImage} className='img-fluid' alt={title} />;
  }

  return (
    <BrandLogoWrap {...props}>
      <a href={path}>
        <div className='brand-logo__image'>{brandImg}</div>
      </a>
    </BrandLogoWrap>
  );
};

ClientLogo.propTypes = {
  path: PropTypes.string,
  title: PropTypes.string,
  image: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
};

ClientLogo.defaultProps = {
  layout: 1,
  path: "/",
  title: "Brand Logo",
};

export default ClientLogo;
