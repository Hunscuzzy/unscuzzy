import styled from "styled-components";

export const BrandLogoWrap = styled.div`
  position: relative;
  a {
    display: block;
  }
`;
