import React from "react";
import PropTypes from "prop-types";
import Img from "gatsby-image";
import { MdTrendingFlat } from "react-icons/md";
import Image from "../../image";
import Button from "../../ui/button";
import {
  BoxLargeImgWrap,
  BoxLargeImgInner,
  BoxLargeImgMedia,
  BoxLargeImgLink,
  BoxLargeImgContent,
  HeadingWrap,
  BoxLargeImgBtnWrap,
} from "./box-large-image.style";

const BoxLargeImage = ({ imageSrc, title, path, btnText, ...boxStyles }) => {
  const { boxStyle, contentBoxStyle, headingStyle, btnStyle } = boxStyles;
  let boxImage;
  if (imageSrc) {
    if (imageSrc.fixed && typeof imageSrc.fixed !== "function") {
      boxImage = <Img fixed={imageSrc.fixed} alt={title} loading='eager' />;
    } else if (imageSrc.fluid) {
      boxImage = <Image fluid={imageSrc.fluid} alt={title} loading='eager' />;
    } else {
      boxImage = <img src={imageSrc} alt={title} />;
    }
  }

  return (
    <BoxLargeImgWrap {...boxStyle}>
      <BoxLargeImgInner>
        {imageSrc && (
          <BoxLargeImgMedia>
            {boxImage}
            <BoxLargeImgLink path={path}>{title}</BoxLargeImgLink>
          </BoxLargeImgMedia>
        )}
        <BoxLargeImgContent {...contentBoxStyle}>
          {title && (
            <HeadingWrap as='h5' {...headingStyle}>
              {title}
            </HeadingWrap>
          )}
          <BoxLargeImgBtnWrap>
            <Button
              {...btnStyle}
              className='button'
              to={path}
              icon={<MdTrendingFlat />}
            >
              {btnText}
            </Button>
          </BoxLargeImgBtnWrap>
        </BoxLargeImgContent>
      </BoxLargeImgInner>
    </BoxLargeImgWrap>
  );
};

BoxLargeImage.propTypes = {
  title: PropTypes.string,
  imageSrc: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  desc: PropTypes.string,
  path: PropTypes.string,
  btnText: PropTypes.string,
  headingStyle: PropTypes.object,
  btnStyle: PropTypes.object,
};

BoxLargeImage.defaultProps = {
  path: "/",
  btnText: "Discover Now",
  btnStyle: {
    iconposition: "right",
    icondistance: "4px",
    varient: "texted",
    hover: "false",
    display: "flex",
    justify: "center",
    alignitems: "center",
    height: "100%",
  },
};

export default BoxLargeImage;
