import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "@emotion/styled";
import { Clients, Testimonials, Text, Code, ImgGallery } from "../slices";
import prism from "../utils/prism";

export default class SliceZone extends Component {
  render() {
    const { allSlices } = this.props;
    const slice = allSlices.map((s) => {
      switch (s.slice_type) {
        // These are the API IDs of the slices
        case "clients":
          return <Clients key={s.id} input={s} />;
        case "temoignages":
          return <Testimonials key={s.id} input={s} />;
        case "text":
          return <Text key={s.id} input={s} />;
        case "snippet":
          return <Code key={s.id} input={s} />;
        case "images_gallery":
          return <ImgGallery key={s.id} input={s} />;
        default:
          return null;
      }
    });
    return <Content>{slice}</Content>;
  }
}

SliceZone.propTypes = {
  allSlices: PropTypes.array.isRequired,
};

const Content = styled.div`
  ${prism};
  h2 {
    font-size: 34px;
  }
  h3 {
    font-size: 28px;
  }
  p,
  li {
    code {
      padding: 0.2rem 0.5rem;
      margin: 0.5rem 0;
    }
  }
  blockquote {
    margin-left: 0;
    padding-left: 1.45rem;
    p {
      font-size: 19px;
      font-style: italic;
    }
  }
`;
