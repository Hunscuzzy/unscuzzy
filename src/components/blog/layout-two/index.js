import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { FaCalendarAlt } from "react-icons/fa";
import Button from "../../ui/button";
import Text from "../../ui/text";
import Anchor from "../../ui/anchor";
import SocialShare from "../social-share";
import BlogMeta from "../blog-meta";
import Categories from "../categories";
import Image from "../../image";

import {
  BlogWrapper,
  BlogMedia,
  BlogThumb,
  BlogInfo,
  BlogHeader,
  BlogTitle,
  BlogHeaderMeta,
  BlogExcerpt,
  BlogFooter,
  BlogFooterLeft,
  BlogFooterRight,
} from "./blog.style";

const Blog = ({ content, ...restProps }) => {
  const {
    data: { date, meta_description, title, thumbnail, categories },
    uid,
  } = content;
  const { metaStyle, categoryBoxStyle } = restProps;

  return (
    <Fragment>
      <BlogWrapper {...restProps}>
        <BlogMedia>
          <BlogThumb>
            <Anchor path={`/blog/${uid}`}>
              <Image
                fluid={thumbnail.localFile.childImageSharp.fluid}
                alt={thumbnail.alt}
              />
            </Anchor>
          </BlogThumb>
        </BlogMedia>
        <BlogInfo>
          <BlogHeader>
            {categories && (
              <Categories {...categoryBoxStyle} categories={categories} />
            )}
            {title && (
              <BlogTitle>
                <Anchor path={`/blog/${uid}`}>{title.text}</Anchor>
              </BlogTitle>
            )}
            <BlogHeaderMeta>
              {date && (
                <BlogMeta {...metaStyle} text={date} icon={<FaCalendarAlt />} />
              )}
            </BlogHeaderMeta>
          </BlogHeader>
          {meta_description && (
            <BlogExcerpt>
              <Text>{meta_description}</Text>
            </BlogExcerpt>
          )}
          <BlogFooter>
            <BlogFooterLeft>
              <Button to={`/blog/${uid}`} hover='2'>
                Lire l'article
              </Button>
            </BlogFooterLeft>
            <BlogFooterRight>
              <SocialShare />
            </BlogFooterRight>
          </BlogFooter>
        </BlogInfo>
      </BlogWrapper>
    </Fragment>
  );
};

Blog.propTypes = {
  content: PropTypes.shape({
    frontmatter: PropTypes.shape({
      title: PropTypes.string,
      author: PropTypes.shape({
        name: PropTypes.string,
        image: PropTypes.object,
      }),
      date: PropTypes.string,
      featured_image: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
      quote_text: PropTypes.string,
      quote_author: PropTypes.string,
      categories: PropTypes.array,
    }),
    excerpt: PropTypes.string,
    fields: PropTypes.shape({
      slug: PropTypes.string,
      authorId: PropTypes.string,
    }),
  }),
  metaStyle: PropTypes.shape({
    mt: PropTypes.string,
    mr: PropTypes.string,
  }),
  categoryBoxStyle: PropTypes.shape({
    mb: PropTypes.string,
  }),
};

Blog.defaultProps = {
  metaStyle: {
    mt: "10px",
    mr: "20px",
  },
  categoryBoxStyle: {
    mb: "18px",
  },
};

export default Blog;
