import React from "react";
import { FaCalendarAlt } from "react-icons/fa";
import Image from "../../image";
import BlogMeta from "../blog-meta";
import Anchor from "../../ui/anchor";
import {
  BlogWrapper,
  BlogMedia,
  BlogThumb,
  BlogInfo,
  BlogHeaderMeta,
  BlogTitle,
} from "./blog.style";
import { ReadMoreBtn } from "../layout-three/blog.style";
import Button from "../../ui/button";
import { MdTrendingFlat } from "react-icons/md";

const Blog = ({ title, slug, date, featured_image, ...restProps }) => {
  const { btnStyle } = restProps;
  return (
    <BlogWrapper>
      <BlogMedia>
        <BlogThumb>
          <Anchor path={`/blog/${slug}`}>
            <Image fluid={featured_image.fluid} alt={title} />
          </Anchor>
        </BlogThumb>
      </BlogMedia>
      <BlogInfo>
        {date && (
          <BlogHeaderMeta>
            <BlogMeta text={date} icon={<FaCalendarAlt />} />
          </BlogHeaderMeta>
        )}
        {title && (
          <BlogTitle>
            <Anchor path={`/blog/${slug}`}>{title}</Anchor>
          </BlogTitle>
        )}
        <ReadMoreBtn>
          <Button to={`/blog/${slug}`} {...btnStyle} icon={<MdTrendingFlat />}>
            Lire l'article
          </Button>
        </ReadMoreBtn>
      </BlogInfo>
    </BlogWrapper>
  );
};

Blog.defaultProps = {
  btnStyle: {
    varient: "texted",
    fontWeight: 400,
    icondistance: "4px",
    hover: "false",
    border: {
      bottom: {
        color: "#ddd",
      },
    },
  },
};

export default Blog;
