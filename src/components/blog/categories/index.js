import React from "react";
import Anchor from "../../ui/anchor";
import { createList } from "../../../utils/utilFunctions";
import { CategroiesWrap } from "./categories.style";

const Categories = ({ categories, ...restProps }) => {
  let catList = createList({
    list: categories,
  });
  return (
    <CategroiesWrap {...restProps}>
      {catList.map((cat, i) => (
        <Anchor path={`/services/${cat.text.category.uid}`} key={i}>
          {cat.text.category.uid}
        </Anchor>
      ))}
    </CategroiesWrap>
  );
};

export default Categories;
