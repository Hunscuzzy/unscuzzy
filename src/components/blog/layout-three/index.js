import React from "react";
import { FaCalendarAlt } from "react-icons/fa";
import { MdTrendingFlat } from "react-icons/md";
import Image from "../../image";
import Button from "../../ui/button";
import Anchor from "../../ui/anchor";
import BlogMeta from "../blog-meta";
import {
  BlogWrapper,
  BlogMedia,
  BlogThumb,
  BlogHeaderMeta,
  BlogInfo,
  BlogTitle,
  BlogExcerpt,
  ReadMoreBtn,
} from "./blog.style";

const Blog = ({
  title,
  slug,
  date,
  author,
  featured_image,
  excerpt,
  ...restProps
}) => {
  const { metaStyle, btnStyle } = restProps;

  return (
    <BlogWrapper>
      <BlogMedia>
        <BlogThumb>
          <Anchor path={`/${slug}`}>
            <Image fluid={featured_image.fluid} alt={title} />
          </Anchor>
        </BlogThumb>
        <BlogHeaderMeta>
          {date && (
            <BlogMeta {...metaStyle} text={date} icon={<FaCalendarAlt />} />
          )}
        </BlogHeaderMeta>
      </BlogMedia>
      <BlogInfo>
        {title && (
          <BlogTitle>
            <Anchor path={`/blog/${slug}`}>{title}</Anchor>
          </BlogTitle>
        )}
        {excerpt && <BlogExcerpt>{excerpt}</BlogExcerpt>}
        <ReadMoreBtn>
          <Button to={`/blog/${slug}`} {...btnStyle} icon={<MdTrendingFlat />}>
            Lire l'article
          </Button>
        </ReadMoreBtn>
      </BlogInfo>
    </BlogWrapper>
  );
};

Blog.defaultProps = {
  metaStyle: {
    color: "#fff",
    ml: "8px",
    mr: "8px",
  },
  btnStyle: {
    varient: "texted",
    fontWeight: 400,
    icondistance: "4px",
    hover: "false",
    border: {
      bottom: {
        color: "#ddd",
      },
    },
  },
};

export default Blog;
