import React from "react";
import { useStaticQuery, graphql } from "gatsby";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel,
} from "react-accessible-accordion";
import AccordionWrap from "../../../components/ui/accordion";
import SectionTitle from "../../../components/ui/section-title";
import PopupVideo from "../../../components/ui/popup-video/layout-one";
import { Container, Row, Col } from "../../../components/ui/wrapper";
import { SectionWrap } from "./style";

const FaqSection = ({ accordionStyle }) => {
  const getFaqData = useStaticQuery(graphql`
    query GET_FAQ_SECTION_ONE_DATA {
      intro: faqJson(id: { eq: "section-one-itro" }) {
        title
      }
      faq: faqJson(id: { eq: "faq" }) {
        items {
          id
          question
          answer
        }
      }
      video: faqJson(id: { eq: "faq-video" }) {
        image {
          childImageSharp {
            fluid(
              maxWidth: 570
              maxHeight: 350
              quality: 90
              srcSetBreakpoints: 6
            ) {
              ...GatsbyImageSharpFluid_withWebp_tracedSVG
              presentationWidth
              presentationHeight
            }
          }
        }
        video_link
      }
      prismicAbout {
        data {
          faq {
            faq_content {
              html
            }
            faq_title1
          }
          faq_title {
            text
          }
          faq_video_url {
            url
          }
          faq_video_image {
            localFile {
              childImageSharp {
                fluid(maxWidth: 570, maxHeight: 350, quality: 90) {
                  ...GatsbyImageSharpFluid_withWebp_tracedSVG
                  presentationWidth
                  presentationHeight
                }
              }
            }
          }
        }
      }
    }
  `);
  const {
    faq,
    faq_title,
    faq_video_url,
    faq_video_image,
  } = getFaqData.prismicAbout.data;
  return (
    <SectionWrap>
      <Container>
        <Row>
          <Col lg={12}>
            <SectionTitle mb='44px' title={faq_title.text} />
          </Col>
        </Row>
        <Row>
          <Col lg={6}>
            <AccordionWrap {...accordionStyle}>
              <Accordion allowZeroExpanded preExpanded={[0]}>
                {faq.map((el, index) => {
                  return (
                    <AccordionItem id={el.id} key={index}>
                      <AccordionItemHeading>
                        <AccordionItemButton>
                          {el.faq_title1}
                        </AccordionItemButton>
                      </AccordionItemHeading>
                      <AccordionItemPanel
                        dangerouslySetInnerHTML={{
                          __html: el.faq_content.html,
                        }}
                      />
                    </AccordionItem>
                  );
                })}
              </Accordion>
            </AccordionWrap>
          </Col>
          <Col lg={6}>
            <PopupVideo
              image={faq_video_image.localFile.childImageSharp}
              video_link={faq_video_url.url}
            />
          </Col>
        </Row>
      </Container>
    </SectionWrap>
  );
};

FaqSection.defaultProps = {
  accordionStyle: {
    responsive: {
      medium: {
        mb: "50px",
      },
    },
  },
};

export default FaqSection;
