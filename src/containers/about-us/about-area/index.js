import React from "react";
import PropTypes from "prop-types";
import { useStaticQuery, graphql } from "gatsby";
import { Container, Row, Col } from "../../../components/ui/wrapper";
import Heading from "../../../components/ui/heading";
import SectionTitle from "../../../components/ui/section-title";
import { AboutWrapper, LeftBox, RightBox } from "./about-area.style";

const AboutDesc = ({
  sectionTitleStyle,
  headingStyle,
  textStyle,
  descStyle,
  btnStyle,
}) => {
  const AboutDescData = useStaticQuery(graphql`
    query AboutPageDescQuery {
      prismicAbout {
        data {
          about_texte {
            html
          }
          about_title {
            html
          }
        }
      }
    }
  `);
  const { about_texte, about_title } = AboutDescData.prismicAbout.data;
  return (
    <AboutWrapper>
      <Container>
        <Row>
          <Col lg={7} md={9} ml='auto' mr='auto'>
            <SectionTitle {...sectionTitleStyle} title={about_title.html} />
          </Col>
        </Row>
        <Row>
          <Col lg={{ span: 4, offset: 1 }}>
            <LeftBox>
              <Heading {...headingStyle}>
                <mark>+7</mark> années d'expérience
              </Heading>
              <Heading {...textStyle}>
                Unscuzzy imagine et développe des sites internet depuis 2014
              </Heading>
            </LeftBox>
          </Col>
          <Col lg={{ span: 5, offset: 1 }}>
            <RightBox>
              {about_texte && (
                <div
                  {...descStyle}
                  dangerouslySetInnerHTML={{ __html: about_texte.html }}
                ></div>
              )}
            </RightBox>
          </Col>
        </Row>
      </Container>
    </AboutWrapper>
  );
};

AboutDesc.propTypes = {
  sectionStyle: PropTypes.object,
  sectionTitleStyle: PropTypes.object,
  headingStyle: PropTypes.object,
  textStyle: PropTypes.object,
  descStyle: PropTypes.object,
};

AboutDesc.defaultProps = {
  sectionTitleStyle: {
    mb: "70px",
    responsive: {
      small: {
        mb: "30px",
      },
    },
  },
  headingStyle: {
    as: "h3",
    layout: "highlight",
    maxwidth: "330px",
    mb: "24px",
    responsive: {
      medium: {
        maxwidth: "100%",
      },
    },
  },
  textStyle: {
    as: "h4",
    fontSize: "20px",
  },
  descStyle: {
    fontSize: "18px",
    lineHeight: 1.67,
    mb: "23px",
  },
  btnStyle: {
    varient: "texted",
    iconname: "far fa-long-arrow-right",
    hover: "false",
  },
};

export default AboutDesc;
