import React from "react";
import PropTypes from "prop-types";
import { useStaticQuery, graphql } from "gatsby";
import { Container, Row, Col } from "../../../components/ui/wrapper";
import SectionTitle from "../../../components/ui/section-title";
import CaseStudyBox from "../../../components/box-large-image/layout-two";
import { SectionWrap, ProjectsWrapper } from "./case-study-area.style";

const CaseStudyArea = ({ sectionTitleStyle, projects, subtitle }) => {
  const caseStudyData = useStaticQuery(graphql`
    query InfoTechCaseStudyQuery {
      indexInfotechnoJson(id: { eq: "infotechno-case-study-content" }) {
        bgImage {
          childImageSharp {
            fluid(quality: 90) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  `);
  const imageData =
    caseStudyData.indexInfotechnoJson.bgImage.childImageSharp.fluid;

  return (
    <SectionWrap fluid={imageData}>
      <Container>
        <Row>
          <Col lg={12}>
            <SectionTitle
              {...sectionTitleStyle}
              title='Quelques unes de nos <span>créations</span>'
              subtitle='Portfolio'
              html={subtitle}
            />
          </Col>
        </Row>
        <Row>
          <Col lg={12}>
            <ProjectsWrapper>
              {projects.map((project, i) => (
                <div className='item' key={i}>
                  <CaseStudyBox
                    imageSrc={
                      project.node.data.thumbnail.localFile.childImageSharp
                    }
                    title={project.node.data.title.text}
                    path={`/portfolio/${project.node.uid}`}
                    btnText='Découvrir le projet'
                  />
                </div>
              ))}
            </ProjectsWrapper>
          </Col>
        </Row>
      </Container>
    </SectionWrap>
  );
};

CaseStudyArea.propTypes = {
  sectionStyle: PropTypes.object,
  sectionTitleStyle: PropTypes.object,
  bottomTextStyle: PropTypes.object,
  bottoTextLinkStyle: PropTypes.object,
};

CaseStudyArea.defaultProps = {
  sectionTitleStyle: {
    mb: "40px",
    responsive: {
      small: {
        mb: "27px",
      },
    },
  },
  bottomTextStyle: {
    fontSize: "18px",
    fontweight: 500,
    lineHeight: 1.4,
    color: "#333333",
    mt: "60px",
    align: "center",
    responsive: {
      small: {
        mt: "45px",
      },
    },
  },
  bottoTextLinkStyle: {
    fontWeight: 500,
    layout: "underline",
    hover: {
      layout: 2,
    },
  },
  slider: {
    slidesPerView: 3,
    loop: false,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
      },
      768: {
        slidesPerView: 2,
      },
      992: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 3,
      },
    },
  },
  sliderStyle: {
    pagination: {
      bottom: 0,
    },
  },
  caseStudyStyles: {
    boxStyle: {
      mt: "20px",
      mb: "35px",
      ml: "15px",
      mr: "15px",
    },
    contentBoxStyle: {
      pt: "25px",
      pl: "26px",
      pr: "26px",
      textalign: "left",
    },
    headingStyle: {
      as: "h6",
      fontweight: 600,
      mb: "2px",
    },
    descStyle: {
      mb: 0,
      mt: "13px",
    },
  },
};

export default CaseStudyArea;
