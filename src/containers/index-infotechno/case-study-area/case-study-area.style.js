import styled from "styled-components";
import { device } from "../../../theme";
import BackgroundImage from "gatsby-background-image";

export const SectionWrap = styled(BackgroundImage)`
  padding-top: 50px;
  padding-bottom: 50px;
  background-position: bottom left;
  background-size: auto;
`;

export const ProjectsWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  & > * {
    flex: 1;
    max-width: 480px;
  }
  .item {
    margin: 25px;
    min-width: 300px;
  }
`;
