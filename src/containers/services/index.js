import React from "react";
import PropTypes from "prop-types";
import { useStaticQuery, graphql } from "gatsby";
import Heading from "../../components/ui/heading";
import Anchor from "../../components/ui/anchor";
import { Container, Row, Col } from "../../components/ui/wrapper";
import SectionTitle from "../../components/ui/section-title";
import ServiceBox from "../../components/box-large-image/layout-one";
import { SectionWrap, Subtitle } from "./services-area.style";

const ServicesArea = (props) => {
  const servicesDataQuery = useStaticQuery(graphql`
    query ServicesData {
      allPrismicCategories {
        edges {
          node {
            uid
            data {
              meta_description
              title {
                text
              }
              thumbnail {
                localFile {
                  childImageSharp {
                    fluid(maxWidth: 600, maxHeight: 370, quality: 100) {
                      ...GatsbyImageSharpFluid_withWebp
                      presentationHeight
                      presentationWidth
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  `);
  const services = servicesDataQuery.allPrismicCategories.edges;
  const { serviceBoxStyles, subtitle } = props;
  return (
    <SectionWrap>
      <Container>
        <Row>
          <Col lg={12}>
            <Subtitle dangerouslySetInnerHTML={{ __html: subtitle }} />
          </Col>
        </Row>
        <Row>
          {services.map((service) => (
            <Col md={6} className='box-item' key={service.node.uid}>
              <ServiceBox
                {...serviceBoxStyles}
                title={service.node.data.title.text}
                desc={service.node.data.meta_description}
                imageSrc={service.node.data.thumbnail.localFile.childImageSharp}
                path={`/services/${service.node.uid}`}
              />
            </Col>
          ))}
        </Row>
      </Container>
    </SectionWrap>
  );
};

ServicesArea.propTypes = {
  serviceBoxStyles: PropTypes.object,
  headingStyle: PropTypes.object,
  linkStyle: PropTypes.object,
};

ServicesArea.defaultProps = {
  headingStyle: {
    as: "h3",
    fontSize: "18px",
    fontweight: 500,
    lineHeight: 1.4,
    color: "#333333",
  },
  linkStyle: {
    layout: "underline",
    hover: {
      layout: 2,
    },
  },
};

export default ServicesArea;
