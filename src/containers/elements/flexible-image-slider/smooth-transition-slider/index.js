import React from "react";
import { Container, Row, Col } from "../../../../components/ui/wrapper";
import Swiper from "../../../../components/ui/swiper";
import { SectionWrap, Item, Caption } from "./style";
import Image from "gatsby-image";

const SmoothTransitionSlider = (props) => {
  const { images } = props;
  const sliderSettings = {
    slidesPerView: "auto",
    centeredSlides: true,
    spaceBetween: 30,
    loop: false,
    autoplay: true,
    infinite: true,
    delay: 1500,
    speed: 5000,
    paginationStyle: {
      mt: "50px",
    },
  };
  return (
    <SectionWrap>
      <Container>
        <Row>
          <Col lg={12} mb='50px'>
            <Swiper settings={sliderSettings}>
              {images.map((el) => {
                return (
                  <Item>
                    <Image fixed={el.image.localFile.childImageSharp.fixed} />
                    <Caption>{el.caption}</Caption>
                  </Item>
                );
              })}
            </Swiper>
          </Col>
        </Row>
      </Container>
    </SectionWrap>
  );
};

export default SmoothTransitionSlider;
