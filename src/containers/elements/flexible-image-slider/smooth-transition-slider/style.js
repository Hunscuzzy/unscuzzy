import styled from "styled-components";
import { device } from "../../../../theme";

export const SectionWrap = styled.section`
  padding-top: 50px;
  padding-bottom: 50px;
`;

export const Item = styled.div`
  position: relative;
`;

export const Caption = styled.p`
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 9;
  bottom: 0;
  width: 100%;
  height: 40px;
  background: ${(props) => props.theme.colors.themeColor};
  font-size: 1.3rem;
  color: #ffffff;
`;
