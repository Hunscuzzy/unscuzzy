import React from "react";
import { useStaticQuery, graphql } from "gatsby";
import { Container, Row, Col } from "../../../components/ui/wrapper";
import Gradation from "../../../components/ui/gradation";
import { gradations } from "./data";
import {
  SectitonWrap,
  GradationRow,
  SectionTitleWrap,
  StepWrap,
} from "./style";

const GradationArea = ({ titleStyle, subtitleStyle }) => {
  const AboutGradationData = useStaticQuery(graphql`
    query AboutPageGradationQuery {
      prismicAbout {
        data {
          steps_descriptions {
            step_description {
              html
            }
            steps_link {
              uid
            }
          }
          steps_title {
            html
          }
        }
      }
    }
  `);
  const {
    steps_descriptions,
    steps_title,
  } = AboutGradationData.prismicAbout.data;
  return (
    <SectitonWrap>
      <Container>
        <Row>
          <Col md={6}>
            <SectionTitleWrap
              dangerouslySetInnerHTML={{ __html: steps_title.html }}
            />
          </Col>
          <Col md={6}>
            <StepWrap>
              <h3>
                <mark>{`0${steps_descriptions.length}`}</mark>Étapes
              </h3>
            </StepWrap>
          </Col>
        </Row>
        <Row>
          <Col lg={12}>
            <GradationRow>
              {steps_descriptions.map((gradation, i) => (
                <Gradation
                  key={i}
                  number={i + 1}
                  isLast={i === gradations.length - 1}
                  title={gradation.title}
                  desc={gradation.step_description.html}
                  path={gradation.steps_link.uid}
                />
              ))}
            </GradationRow>
          </Col>
        </Row>
      </Container>
    </SectitonWrap>
  );
};

GradationArea.defaultProps = {
  titleStyle: {
    as: "h4",
    child: {
      color: "primary",
    },
  },
  subtitleStyle: {
    as: "h6",
    color: "#333",
    fontSize: "14px",
    letterspacing: "3px",
    texttransform: "uppercase",
    lineHeight: 1.43,
    mt: "-5px",
    mb: "20px",
  },
};

export default GradationArea;
