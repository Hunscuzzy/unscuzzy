import React from "react";
import PropTypes from "prop-types";
import { Container, Row, Col } from "../../../components/ui/wrapper";
import ContactForm from "../../../components/forms/contact-form";
import { ContactFormWrapper } from "./contact-form-area.style";
import ContactInfoArea from "../contact-info-area";

const ContactFormArea = () => {
  return (
    <ContactFormWrapper>
      <Container>
        <Row>
          <Col lg={6}>
            <ContactInfoArea />
          </Col>
          <Col lg={6}>
            <ContactForm />
          </Col>
        </Row>
      </Container>
    </ContactFormWrapper>
  );
};

ContactFormArea.propTypes = {
  headingStyle: PropTypes.object,
  textStyle: PropTypes.object,
};

ContactFormArea.defaultProps = {
  headingStyle: {
    as: "h3",
    position: "relative",
    pl: "34px",
    lineheight: 1.67,
    fontweight: 600,
    child: {
      color: "primary",
    },
    layout: "quote",
  },
  textStyle: {
    mt: "15px",
    fontSize: "18px",
    ml: "34px",
    color: "#696969",
  },
};

export default ContactFormArea;
