import React from "react";
import { Container, Row, Col } from "../../components/ui/wrapper";
import Button from "../../components/ui/button";
import { ErrorWrap } from "./error-area.style";

const ErroArea = () => {
  return (
    <ErrorWrap>
      <Container>
        <Row>
          <Col xl={7} lg={8} ml='auto' mr='auto'>
            <h1>404</h1>
            <h2>OOPS! LA PAGE N'EXISTE PAS/PLUS</h2>
            <p>
              Désolé, la page à laquelle vous tentez d'accéder n'existe pas ou a
              été déplacée.
            </p>
            <Button to='/' hover='false'>
              Retourner à l'accueil
            </Button>
          </Col>
        </Row>
      </Container>
    </ErrorWrap>
  );
};

export default ErroArea;
