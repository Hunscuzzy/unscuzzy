import React from "react";
import PropTypes from "prop-types";
import { useStaticQuery, graphql } from "gatsby";
import {
  TiSocialFacebook,
  TiSocialInstagram,
  TiSocialLinkedin,
} from "react-icons/ti";
import parse from "html-react-parser";
import { Container } from "react-bootstrap";
import Logo from "../../../assets/images/logo/dark-logo.png";
import { Row, Col } from "../../../components/ui/wrapper";
import Text from "../../../components/ui/text";
import Anchor from "../../../components/ui/anchor";
import Heading from "../../../components/ui/heading";
import Social, { SocialLink } from "../../../components/ui/social";
import {
  FooterWrap,
  FooterTop,
  FooterWidget,
  LogoWidget,
  TextWidget,
  FooterWidgetList,
  FooterBottom,
} from "./footer.style";

const Footer = ({ copyrightStyle, ...props }) => {
  const siteInfo = useStaticQuery(graphql`
    query FooterSiteQuery {
      site {
        siteMetadata {
          siteUrl
          description
          copyright
          contact {
            phone
            email
            address
            website
          }
          social {
            facebook
            instagram
            linkedin
          }
        }
      }
      allPrismicLegals {
        edges {
          node {
            uid
            data {
              title {
                text
              }
            }
          }
        }
      }
      allPrismicCategories {
        edges {
          node {
            uid
            data {
              title {
                text
              }
            }
          }
        }
      }
      allPrismicBlogPost(limit: 5) {
        edges {
          node {
            uid
            data {
              title {
                text
              }
            }
          }
        }
      }
    }
  `);
  const { phone, email, address, website } = siteInfo.site.siteMetadata.contact;
  const { copyright, description } = siteInfo.site.siteMetadata;
  const { facebook, instagram, linkedin } = siteInfo.site.siteMetadata.social;
  const articles = siteInfo.allPrismicBlogPost.edges;
  return (
    <FooterWrap {...props}>
      <FooterTop>
        <Container>
          <Row>
            <Col lg={4} sm={6}>
              <FooterWidget responsive={{ medium: { mb: "31px" } }}>
                <LogoWidget>
                  <Anchor path={website} hoverstyle='2'>
                    <img src={Logo} alt='Logo' />
                  </Anchor>
                </LogoWidget>
                <Text italic>{description}</Text>

                <TextWidget>
                  {address && <Text mb='10px'>{address}</Text>}
                  {email && (
                    <Text mb='10px'>
                      <Anchor
                        path={`mailto:${email}`}
                        color='textColor'
                        hoverstyle='2'
                      >
                        {email}
                      </Anchor>
                    </Text>
                  )}
                  {phone && (
                    <Text mb='10px'>
                      <Anchor
                        path={`tel:${phone}`}
                        fontWeight='800'
                        color='#333'
                        hoverstyle='2'
                      >
                        {phone}
                      </Anchor>
                    </Text>
                  )}
                </TextWidget>
              </FooterWidget>
            </Col>
            <Col lg={3} md={4} sm={6}>
              <FooterWidget responsive={{ medium: { mb: "27px" } }}>
                <Heading as='h6' mt='-3px' mb='20px'>
                  Articles de blog
                </Heading>
                <FooterWidgetList>
                  {articles.map((article) => {
                    return (
                      <li key={article.node.uid}>
                        <Anchor
                          path={`/blog/${article.node.uid}`}
                          color='textColor'
                          hoverstyle='2'
                        >
                          {article.node.data.title.text}
                        </Anchor>
                      </li>
                    );
                  })}
                </FooterWidgetList>
              </FooterWidget>
            </Col>
            <Col lg={2} md={4} sm={6}>
              <FooterWidget responsive={{ medium: { mb: "31px" } }}>
                <Heading as='h6' mt='-3px' mb='20px'>
                  Nos services
                </Heading>
                <FooterWidgetList>
                  {siteInfo.allPrismicCategories.edges.map((page) => {
                    return (
                      <li key={page.node.uid}>
                        <Anchor
                          path={page.node.uid}
                          color='textColor'
                          hoverstyle='2'
                        >
                          {page.node.data.title.text}
                        </Anchor>
                      </li>
                    );
                  })}
                </FooterWidgetList>
              </FooterWidget>
            </Col>
            <Col lg={2} md={4} sm={6}>
              <FooterWidget>
                <Heading as='h6' mt='-3px' mb='20px'>
                  À propos
                </Heading>
                <FooterWidgetList>
                  <li>
                    <Anchor path='/agence-web' color='textColor' hoverstyle='2'>
                      L'Agence
                    </Anchor>
                  </li>
                  {siteInfo.allPrismicLegals.edges.map((page) => {
                    return (
                      <li key={page.node.uid}>
                        <Anchor
                          path={`/${page.node.uid}`}
                          color='textColor'
                          hoverstyle='2'
                        >
                          {page.node.data.title.text}
                        </Anchor>
                      </li>
                    );
                  })}
                  <li>
                    <Anchor path='/contact' color='textColor' hoverstyle='2'>
                      Contact
                    </Anchor>
                  </li>
                </FooterWidgetList>
              </FooterWidget>
            </Col>
          </Row>
        </Container>
      </FooterTop>
      <FooterBottom>
        <Container>
          <Row className='align-items-center'>
            <Col md={6} className='text-center text-md-left'>
              {copyright && (
                <Text {...copyrightStyle}>
                  &copy; {new Date().getFullYear()} {parse(copyright)}
                </Text>
              )}
            </Col>
            <Col md={6} className='text-center text-md-right'>
              <Social
                space='8px'
                tooltip={true}
                shape='rounded'
                varient='outlined'
              >
                {facebook && (
                  <SocialLink path={facebook} title='Facebook'>
                    <TiSocialFacebook />
                  </SocialLink>
                )}
                {instagram && (
                  <SocialLink path={instagram} title='Instagram'>
                    <TiSocialInstagram />
                  </SocialLink>
                )}
                {linkedin && (
                  <SocialLink path={linkedin} title='Linkedin'>
                    <TiSocialLinkedin />
                  </SocialLink>
                )}
              </Social>
            </Col>
          </Row>
        </Container>
      </FooterBottom>
    </FooterWrap>
  );
};

Footer.propTypes = {
  bgcolor: PropTypes.string,
  reveal: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

Footer.defaultProps = {
  bgcolor: "#F8F8F8",
  reveal: "false",
  copyrightStyle: {
    responsive: {
      small: {
        pb: "15px",
      },
    },
  },
};

export default Footer;
