import React from "react";
import { MdTrendingFlat } from "react-icons/md";
import { List, ListItem, ListLink } from "./recent-post-list.style";

const RecentPostList = ({ posts }) => {
  return (
    <List>
      <h1>posttt listtt</h1>
      {posts &&
        posts.map((latestPost, i) => (
          <ListItem key={i}>
            <ListLink path={`/${latestPost.node.uid}`}>
              <MdTrendingFlat className='icon icon-1' />
              <MdTrendingFlat className='icon icon-2' />
              <span>{latestPost.node.data.title[0].text}</span>
            </ListLink>
          </ListItem>
        ))}
    </List>
  );
};

export default RecentPostList;
