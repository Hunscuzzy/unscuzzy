import React from "react";
import PropTypes from "prop-types";
import { useStaticQuery, graphql } from "gatsby";
import { Box } from "../../../components/ui/wrapper";
import Button from "../../../components/ui/button";

const PopularTags = ({ boxStyle, tagStyle }) => {
  const BlogTagsQuery = useStaticQuery(graphql`
    query BlogSidebarTagsQuery {
      allPrismicCategories {
        edges {
          node {
            uid
            data {
              title {
                text
              }
            }
          }
        }
      }
    }
  `);
  const tags = BlogTagsQuery.allPrismicCategories.edges;

  return (
    <Box {...boxStyle}>
      {tags.map((tag, i) => {
        return (
          <Button {...tagStyle} to={`/services/${tag.node.uid}`} key={i}>
            {tag.node.data.title.text}
          </Button>
        );
      })}
    </Box>
  );
};

PopularTags.propTypes = {
  tagStyle: PropTypes.object,
};

PopularTags.defaultProps = {
  boxStyle: {
    m: "-5px",
  },
  tagStyle: {
    hover: "2",
    size: "xsmall",
    skin: "light",
    color: "#ababab",
    bgcolor: "#f5f5f5",
    p: "0 14px",
    m: "5px",
    height: "32px",
    lineheight: "32px",
    fontSize: "13px",
  },
};

export default PopularTags;
