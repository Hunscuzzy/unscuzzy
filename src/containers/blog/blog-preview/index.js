import React from "react";
import { Container, Row, Col } from "../../../components/ui/wrapper";
import SectionTitle from "../../../components/ui/section-title";
import FeaturedBlog from "../../../components/blog/layout-three";
import RecentBlog from "../../../components/blog/layout-four";
import { BlogWrap, BlogList, BlogListItem } from "./blog-area.style";

const BlogArea = (props) => {
  const { sectionTitleStyle, featureBlogs, recentBlogs } = props;
  return (
    <BlogWrap>
      <Container>
        <Row>
          <Col lg={12}>
            <SectionTitle
              {...sectionTitleStyle}
              title='Qui pourront de <span>vous intéresser</span>'
              subtitle='Quelques articles de blog'
            />
          </Col>
        </Row>
        <Row>
          <Col lg={6}>
            {featureBlogs &&
              featureBlogs.map((featureBlog, i) => (
                <FeaturedBlog
                  key={i}
                  title={featureBlog.node.data.title.text}
                  slug={featureBlog.node.uid}
                  date={featureBlog.node.data.date}
                  featured_image={
                    featureBlog.node.data.thumbnail.localFile.childImageSharp
                  }
                  excerpt={featureBlog.node.data.meta_description}
                />
              ))}
          </Col>
          <Col lg={6}>
            <BlogList>
              {recentBlogs &&
                recentBlogs.map((recentBlog, i) => (
                  <BlogListItem key={i}>
                    <RecentBlog
                      title={recentBlog.node.data.title.text}
                      slug={recentBlog.node.uid}
                      date={recentBlog.node.data.date}
                      featured_image={
                        recentBlog.node.data.thumbnail.localFile.childImageSharp
                      }
                    />
                  </BlogListItem>
                ))}
            </BlogList>
          </Col>
        </Row>
      </Container>
    </BlogWrap>
  );
};

BlogArea.defaultProps = {
  sectionTitleStyle: {
    mb: "60px",
    responsive: {
      small: {
        mb: "47px",
      },
    },
  },
};

export default BlogArea;
