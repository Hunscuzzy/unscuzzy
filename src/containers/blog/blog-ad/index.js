import React from "react";
import { useStaticQuery, graphql } from "gatsby";
import Anchor from "../../../components/ui/anchor";
import Image from "../../../components/image";

const BlogAd = ({ image, link }) => {
  const imageData = image.localFile.childImageSharp.fluid;
  console.log(link);
  return (
    <Anchor path={``} display='block'>
      <Image fluid={imageData} alt='Projet Unscuzzy' />
    </Anchor>
  );
};

export default BlogAd;
