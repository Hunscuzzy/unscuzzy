import React from "react";
import PropTypes from "prop-types";
import Heading from "../../../components/ui/heading";
import BlogAd from "../blog-ad";
import PopularTags from "../popular-tags";
import { SidebarWrap, AdWidgetBox, TagWidgetBox } from "./sidebar.style";

const Sidebar = ({ widgetTitleStyle, pub }) => {
  return (
    <SidebarWrap>
      <TagWidgetBox mb='50px'>
        <Heading {...widgetTitleStyle}>Catégories</Heading>
        <PopularTags />
      </TagWidgetBox>
      {pub &&
        pub.map((el, i) => (
          <AdWidgetBox key={i}>
            <BlogAd image={el.pub_image} link={el.link.url} />
          </AdWidgetBox>
        ))}
    </SidebarWrap>
  );
};

Sidebar.propTypes = {
  widgetTitleStyle: PropTypes.object,
};

Sidebar.defaultProps = {
  widgetTitleStyle: {
    fontSize: "34px",
    fontweight: 500,
    mb: "27px",
  },
};

export default Sidebar;
