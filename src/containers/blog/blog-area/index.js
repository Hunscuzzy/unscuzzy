import React, { Fragment } from "react";
import { useStaticQuery, graphql } from "gatsby";
import Blog from "../../../components/blog/layout-two";
import Pagination from "../../../components/blog/pagination";
import { BlogWrapper, BlogBox } from "./blog-area.style";

const BlogArea = () => {
  const BlogQuery = useStaticQuery(graphql`
    query ListImageBlogQuery {
      countBlog: allPrismicBlogPost {
        edges {
          node {
            uid
          }
        }
      }
      blogContents: allPrismicBlogPost(
        sort: { fields: data___date, order: DESC }
        limit: 6
      ) {
        edges {
          node {
            data {
              date
              meta_description
              title {
                text
              }
              thumbnail {
                localFile {
                  childImageSharp {
                    fluid(maxWidth: 770, maxHeight: 420, quality: 90) {
                      ...GatsbyImageSharpFluid_withWebp
                      presentationHeight
                      presentationWidth
                    }
                  }
                }
                alt
              }
            }
            uid
          }
        }
      }
    }
  `);
  const blogs = BlogQuery.blogContents.edges;
  const countBlog = BlogQuery.countBlog.edges;
  const totalCount = countBlog.length;
  const postsPerPage = 6;
  const numberOfPages = Math.ceil(totalCount / postsPerPage);
  return (
    <Fragment>
      <BlogWrapper>
        {blogs.map((blog) => (
          <BlogBox key={blog.node.uid}>
            <Blog content={blog.node} />
          </BlogBox>
        ))}
      </BlogWrapper>
      <Pagination
        rootPage='/blog'
        currentPage={1}
        numberOfPages={numberOfPages}
      />
    </Fragment>
  );
};

export default BlogArea;
