import React from "react";
import PropTypes from "prop-types";
import { Container, Row, Col } from "../../../components/ui/wrapper";
import ClientLogo from "../../../components/ui/client-logo";
import SwiperSlider from "../../../components/ui/swiper";
import { SectionWrap, ClientLogoWrap } from "./style";
import SectionTitle from "../../../components/ui/section-title";

const ClientsArea = (props) => {
  const { slider, sliderStyle, clients, sectionTitleStyle } = props;
  return (
    <SectionWrap>
      <Container>
        <Row>
          <Col lg={12}>
            <SectionTitle
              {...sectionTitleStyle}
              title='Nous avons travaillé <span>ensemble</span>'
              subtitle='Références'
            />
          </Col>
        </Row>
        <Row>
          <Col lg={12}>
            <SwiperSlider settings={slider} {...sliderStyle}>
              {clients.map((data, i) => {
                return (
                  <ClientLogoWrap key={i}>
                    <ClientLogo
                      layout={1}
                      title={data.logo.alt}
                      path={data.lien.url}
                      brandImage={data.logo.localFile.childImageSharp}
                    />
                  </ClientLogoWrap>
                );
              })}
            </SwiperSlider>
          </Col>
        </Row>
      </Container>
    </SectionWrap>
  );
};

ClientsArea.propTypes = {
  slider: PropTypes.object,
  sliderStyle: PropTypes.object,
  image: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
};

ClientsArea.defaultProps = {
  slider: {
    slidesPerView: 6,
    speed: 1000,
    pagination: false,
    autoplay: {
      delay: 2500,
    },
    breakpoints: {
      320: {
        slidesPerView: 2,
      },
      575: {
        slidesPerView: 3,
      },
      767: {
        slidesPerView: 4,
      },
      991: {
        slidesPerView: 5,
      },
      1499: {
        slidesPerView: 6,
      },
    },
  },
  sliderStyle: {
    align: "center",
  },
  sectionTitleStyle: {
    mb: "40px",
    responsive: {
      small: {
        mb: "30px",
      },
    },
  },
};

export default ClientsArea;
