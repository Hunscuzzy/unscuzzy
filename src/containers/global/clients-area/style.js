import styled from "styled-components";

export const SectionWrap = styled.div`
  padding: 50px 0;
`;

export const ClientLogoWrap = styled.div`
  align-self: center;
  text-align: center;
  position: relative;
`;
