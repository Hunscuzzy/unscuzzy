import React from "react";
import PropTypes from "prop-types";
import { useStaticQuery, graphql } from "gatsby";
import { MdPhone } from "react-icons/md";
import { Container, Row, Col, Box } from "../../../../components/ui/wrapper";
import Heading from "../../../../components/ui/heading";
import Text from "../../../../components/ui/text";
import Button from "../../../../components/ui/button";
import { SectionWrap, ContactInfoBox } from "./contact.style";

const ContactArea = ({
  headingStyle,
  descStyle,
  conactInfoStyles: { titleStyle, phoneAnchorStyle, btnStyle },
}) => {
  const contactQueryData = useStaticQuery(graphql`
    query {
      sectionBg: file(relativePath: { eq: "images/bg/jacque-rafael-2.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 1192, maxHeight: 630, quality: 90) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      contactData: site {
        siteMetadata {
          contact {
            phone
          }
        }
      }
    }
  `);
  const imageData = contactQueryData.sectionBg.childImageSharp.fluid;
  const { phone } = contactQueryData.contactData.siteMetadata.contact;
  return (
    <SectionWrap fluid={imageData}>
      <Container>
        <Row alignitems='center'>
          <Col lg={6}>
            <Box>
              <Heading {...headingStyle}>
                Contactez-nous par téléphone ou par email
              </Heading>
              <Text {...descStyle}>
                Disponible 6 jours sur 7, 8 heures par jour : Notre
                disponibilité et notre réactivité sont à vos côtés.
              </Text>
            </Box>
          </Col>
          <Col lg={6}>
            <ContactInfoBox>
              <MdPhone className='icon' />
              <Heading {...titleStyle}>APPELEZ-NOUS!</Heading>
              {phone && (
                <Heading>
                  <a
                    href={"tel:" + phone}
                    {...phoneAnchorStyle}
                    className='tel'
                  >
                    {phone}
                  </a>
                </Heading>
              )}
              <Button {...btnStyle} to='/contact'>
                Formulaire de contact
              </Button>
            </ContactInfoBox>
          </Col>
        </Row>
      </Container>
    </SectionWrap>
  );
};

ContactArea.propTypes = {
  headingStyle: PropTypes.object,
};

ContactArea.defaultProps = {
  headingStyle: {
    as: "h3",
    position: "relative",
    pl: "34px",
    fontweight: 600,
    lineHeight: 1.4,
    color: "#fff",
    before: {
      top: "50%",
      width: "4px",
      height: "94%",
      bgColor: "secondary",
      transform: "translateY(-50%)",
    },
  },
  descStyle: {
    mt: "15px",
    fontSize: "18px",
    color: "#fff",
    ml: "34px",
  },
  conactInfoStyles: {
    titleStyle: {
      as: "h6",
      fontSize: "15px",
      color: "#fff",
      letterspacing: "2px",
      texttransform: "uppercase",
      mb: "10px",
      mt: "10px",
    },
    btnStyle: {
      mt: "20px",
      skin: "light",
      minwidth: "230px",
      color: "primary",
      hover: {
        bgColor: "secondary",
      },
    },
  },
};

export default ContactArea;
