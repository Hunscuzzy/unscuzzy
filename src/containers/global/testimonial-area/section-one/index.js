import React from "react";
import PropTypes from "prop-types";
import { Container, Row, Col } from "../../../../components/ui/wrapper";
import SectionTitle from "../../../../components/ui/section-title";
import SwiperSlider from "../../../../components/ui/swiper";
import Testimonial from "../../../../components/testimonial/layout-one";
import { TestimonialWrapper } from "./section.style";

const TestimonialSection = (props) => {
  const { sectionTitleStyle, slider, sliderStyle, testimonials } = props;
  return (
    <TestimonialWrapper>
      <Container>
        <Row>
          <Col lg={12}>
            <SectionTitle
              {...sectionTitleStyle}
              title="Que pensent-t-ils d'<span>Unscuzzy?</span>"
              subtitle='Témoignages'
            />
          </Col>
        </Row>
        <Row>
          <Col lg={12}>
            <SwiperSlider {...sliderStyle} settings={slider}>
              {testimonials.map((testimonial, i) => (
                <div className='item' key={i}>
                  <Testimonial
                    authorName={testimonial.nom.text}
                    authorRole={testimonial.entreprise.text}
                    authorImg={
                      testimonial.logo___photo.localFile &&
                      testimonial.logo___photo.localFile.childImageSharp
                    }
                    review={testimonial.citation.html}
                  />
                </div>
              ))}
            </SwiperSlider>
          </Col>
        </Row>
      </Container>
    </TestimonialWrapper>
  );
};

TestimonialSection.propTypes = {
  sliderStyle: PropTypes.object,
  sectionTitleStyle: PropTypes.object,
  slider: PropTypes.object,
};

TestimonialSection.defaultProps = {
  sectionTitleStyle: {
    mb: "40px",
    responsive: {
      small: {
        mb: "30px",
      },
    },
  },
  slider: {
    slidesPerView: 1,
    loop: false,
    autoplay: {
      delay: 4500,
    },
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
      },
    },
  },
  sliderStyle: {
    pagination: {
      mt: "28px",
    },
  },
};

export default TestimonialSection;
