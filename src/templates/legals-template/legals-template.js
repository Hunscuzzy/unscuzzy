import React from "react";
import PropTypes from "prop-types";
import { graphql } from "gatsby";
import SEO from "../../components/seo";
import Layout from "../../containers/layout/layout";
import Header from "../../containers/layout/header";
import Footer from "../../containers/layout/footer";
import PageHeader from "../../components/pageheader";
import { Container, Row, Col } from "../../components/ui/wrapper";
import CTA from "../../containers/global/cta-area/section-one";

const LegalPage = ({ data, pageContext, location, ...restProps }) => {
  const legal = data.prismicLegals;
  return (
    <Layout location={location}>
      <SEO
        title={legal.data.meta_title}
        description={legal.data.meta_description}
        keywords={legal.data.keywords}
      />
      <Header />
      <PageHeader
        pageContext={pageContext}
        location={location}
        title={legal.data.title.text}
        image={legal.data.thumbnail.localFile.childImageSharp}
      />
      <main className='site-wrapper-reveal'>
        <Container>
          <Row>
            <Col
              pt='100px'
              pb='100px'
              dangerouslySetInnerHTML={{ __html: legal.data.contenu.html }}
            />
          </Row>
        </Container>
        <CTA />
      </main>
      <Footer />
    </Layout>
  );
};

export const query = graphql`
  query LegalsBySlug($uid: String!) {
    prismicLegals(uid: { eq: $uid }) {
      data {
        title {
          text
        }
        thumbnail {
          localFile {
            childImageSharp {
              fluid(maxWidth: 1920, maxHeight: 400, quality: 90) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
        keywords
        meta_description
        meta_title
        contenu {
          html
        }
      }
      uid
    }
  }
`;

LegalPage.propTypes = {
  headingStyle: PropTypes.object,
};

export default LegalPage;
