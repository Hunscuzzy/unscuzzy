import React from "react";
import PropTypes from "prop-types";
import { graphql } from "gatsby";
import Layout from "../../containers/layout/layout";
import Header from "../../containers/layout/header";
import Footer from "../../containers/layout/footer";
import { Container, Row, Col } from "../../components/ui/wrapper";
import SEO from "../../components/seo";
import Heading from "../../components/ui/heading";
import CTA from "../../containers/global/cta-area/section-one";
import { formatDate } from "../../utils/utilFunctions";
import SliceZone from "../../components/slicesZone";
import PageHeader from "../../components/pageheader";
import Button from "../../components/ui/button";
import { MdInfoOutline } from "react-icons/md";
import { BlogInfo, HeaderMetaWrap } from "../blog-template/blog-template.stc";
import BlogMeta from "../../components/blog/blog-meta";
import { FaCalendarAlt } from "react-icons/fa";

const CaseStudyTemplate = ({
  data: { prismicPortfolioPost },
  location,
  pageContext,
  ...restProps
}) => {
  const prismicData = prismicPortfolioPost.data;

  const { titleStyle } = restProps;

  return (
    <Layout location={location}>
      <Header />
      <SEO
        title={prismicData.meta_title}
        description={prismicData.meta_description}
      />
      <PageHeader
        pageContext={pageContext}
        location={location}
        title='Portfolio'
        image={prismicData.thumbnail.localFile.childImageSharp}
      />
      <main className='site-wrapper-reveal'>
        {(prismicData.title || prismicData.logo) && (
          <Container>
            <Row justify='center'>
              <Col lg={12} textalign='center' mt='50px' mb='50px'>
                <BlogInfo>
                  {prismicData.title && (
                    <Heading {...titleStyle}>{prismicData.title.text}</Heading>
                  )}
                  <HeaderMetaWrap>
                    {prismicData.date && (
                      <BlogMeta
                        mt='10px'
                        mr='20px'
                        text={"Mis en ligne le " + formatDate(prismicData.date)}
                        icon={<FaCalendarAlt />}
                      />
                    )}
                  </HeaderMetaWrap>
                </BlogInfo>
              </Col>
            </Row>
            <Row>
              <Col lg={12}>
                <SliceZone allSlices={prismicPortfolioPost.data.body} />
              </Col>
            </Row>
            <Row mb='50px'>
              <Col textalign='center'>
                <Button
                  to='prismicData.liens.url'
                  icon={<MdInfoOutline />}
                  iconposition='right'
                  icondistance='4px'
                  iconsize='16px'
                >
                  Découvrir le site
                </Button>
              </Col>
            </Row>
          </Container>
        )}
        <CTA />
      </main>
      <Footer />
    </Layout>
  );
};

export const query = graphql`
  query PostBySlug($uid: String!) {
    prismicPortfolioPost(uid: { eq: $uid }) {
      data {
        meta_title
        meta_description
        liens {
          url
        }
        date
        title {
          text
        }
        thumbnail {
          localFile {
            childImageSharp {
              fluid(maxWidth: 1920, maxHeight: 570, quality: 90) {
                ...GatsbyImageSharpFluid_withWebp
                presentationWidth
                presentationHeight
              }
            }
          }
          alt
        }
        body {
          ... on PrismicPortfolioPostBodyText {
            id
            slice_type
            primary {
              text {
                html
              }
            }
          }
          ... on PrismicPortfolioPostBodyTemoignages {
            id
            slice_type
            items {
              citation {
                html
              }
              entreprise {
                text
              }
              nom {
                text
              }
              logo___photo {
                alt
                localFile {
                  childImageSharp {
                    fixed(width: 90, height: 90, quality: 90) {
                      ...GatsbyImageSharpFixed
                    }
                  }
                }
              }
            }
          }
          ... on PrismicPortfolioPostBodyImagesGallery {
            id
            slice_type
            items {
              caption
              image {
                localFile {
                  childImageSharp {
                    fixed(height: 600, quality: 90) {
                      ...GatsbyImageSharpFixed
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;

CaseStudyTemplate.propTypes = {
  taglineStyle: PropTypes.object,
};

CaseStudyTemplate.defaultProps = {
  titleStyle: {
    as: "h1",
    fontSize: "40px",
    textalign: "center",
    mb: "15px",
    wordwrap: "break-word",
    responsive: {
      medium: {
        fontSize: "35px",
      },
      small: {
        fontSize: "25px",
      },
    },
  },
};

export default CaseStudyTemplate;
