import styled from "styled-components";
import { device } from "../../theme";
import BackgroundImage from "gatsby-background-image";

export const BannerArea = styled(BackgroundImage)`
  padding-top: 85px;
  padding-bottom: 195px;
  @media ${device.large} {
    padding-top: 120px;
    padding-bottom: 120px;
  }
  @media ${device.medium} {
    padding-top: 100px;
    padding-bottom: 100px;
  }
  @media ${device.small} {
    padding-top: 60px;
    padding-bottom: 60px;
  }
`;

export const BannerTextWrap = styled.div`
  text-align: center;
`;
