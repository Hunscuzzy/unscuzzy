import React from "react";
import PropTypes from "prop-types";
import { graphql } from "gatsby";
import { FaCalendarAlt } from "react-icons/fa";
import Layout from "../../containers/layout/layout";
import Header from "../../containers/layout/header";
import Footer from "../../containers/layout/footer";
import Section, { Box, Row, Col } from "../../components/ui/wrapper";
import SEO from "../../components/seo";
import PageHeader from "../../components/pageheader";
import Heading from "../../components/ui/heading";
import PostNav from "../../components/post-nav/layout-two";
import BlogMeta from "../../components/blog/blog-meta";
import SocialShare from "../../components/blog/social-share";
import Categories from "../../components/blog/categories";
import {
  SingleBlogWrap,
  BlogInfo,
  HeaderMetaWrap,
  FooterMetaWrap,
  BlogNavigation,
  CategoryBox,
} from "./blog-template.stc";
import SliceZone from "../../components/slicesZone";

const BlogTemplate = ({
  data: { prismicBlogPost },
  pageContext,
  location,
  ...restProps
}) => {
  const post = prismicBlogPost.data;
  const { next, previous } = pageContext;
  const { sectionStyle, titleStyle, headerMetaStyle } = restProps;

  return (
    <Layout location={location}>
      <Header />
      <SEO title={post.meta_title} description={post.meta_description} />
      <PageHeader
        pageContext={pageContext}
        location={location}
        title='Blog'
        image={post.thumbnail.localFile.childImageSharp}
      />
      <main className='site-wrapper-reveal'>
        <Section {...sectionStyle}>
          <SingleBlogWrap>
            <BlogInfo>
              <Row>
                <Col lg={8} ml='auto' mr='auto'>
                  <CategoryBox>
                    <Categories categories={post.relations} />
                  </CategoryBox>
                  <Heading {...titleStyle}>{post.title.text}</Heading>
                  <HeaderMetaWrap>
                    {post.date && (
                      <BlogMeta
                        {...headerMetaStyle}
                        text={post.date}
                        icon={<FaCalendarAlt />}
                      />
                    )}
                  </HeaderMetaWrap>
                </Col>
              </Row>
            </BlogInfo>
            <SliceZone allSlices={post.body} />
            <Box as='footer'>
              <FooterMetaWrap>
                <Row alignitems='center'>
                  <Col sm={12}>
                    <SocialShare title={post.title} url={prismicBlogPost.uid} />
                  </Col>
                </Row>
              </FooterMetaWrap>
              <BlogNavigation>
                {previous && (
                  <PostNav
                    rel='prev'
                    path={previous.uid}
                    title={previous.data.title.text}
                    image={
                      previous.data.thumbnail
                        ? previous.data.thumbnail.localFile.childImageSharp
                            .fluid
                        : null
                    }
                  />
                )}
                {next && (
                  <PostNav
                    rel='next'
                    path={next.uid}
                    title={next.data.title.text}
                    image={
                      next.data.thumbnail
                        ? next.data.thumbnail.localFile.childImageSharp.fluid
                        : null
                    }
                  />
                )}
              </BlogNavigation>
            </Box>
          </SingleBlogWrap>
        </Section>
      </main>
      <Footer />
    </Layout>
  );
};

BlogTemplate.propTypes = {
  sectionStyle: PropTypes.object,
};

BlogTemplate.defaultProps = {
  sectionStyle: {
    pt: "120px",
    pb: "120px",
    responsive: {
      large: {
        pt: "100px",
        pb: "100px",
      },
      medium: {
        pt: "80px",
        pb: "80px",
      },
      small: {
        pt: "60px",
        pb: "60px",
        fullwidth: true,
      },
    },
  },
  titleStyle: {
    as: "h1",
    fontSize: "40px",
    textalign: "center",
    mb: "15px",
    wordwrap: "break-word",
    responsive: {
      medium: {
        fontSize: "35px",
      },
      small: {
        fontSize: "25px",
      },
    },
  },
  headerMetaStyle: {
    mt: "10px",
    mr: "20px",
  },
};

export const query = graphql`
  query BlogPostBySlug($uid: String!) {
    prismicBlogPost(uid: { eq: $uid }) {
      data {
        date
        meta_description
        meta_title
        thumbnail {
          localFile {
            childImageSharp {
              fluid(maxWidth: 1170, maxHeight: 570, quality: 100) {
                ...GatsbyImageSharpFluid_withWebp
                presentationWidth
                presentationHeight
              }
            }
          }
        }
        title {
          text
        }
        relations {
          category {
            uid
          }
        }
        body {
          ... on PrismicBlogPostBodyText {
            id
            slice_type
            primary {
              text {
                html
              }
            }
          }
          ... on PrismicBlogPostBodyTemoignages {
            id
            slice_type
            items {
              citation {
                html
              }
              entreprise {
                text
              }
              nom {
                text
              }
              logo___photo {
                alt
                localFile {
                  childImageSharp {
                    fixed(width: 90, height: 90, quality: 90) {
                      ...GatsbyImageSharpFixed
                    }
                  }
                }
              }
            }
          }
          ... on PrismicBlogPostBodySnippet {
            slice_type
            primary {
              code {
                text
              }
              language
            }
          }
          ... on PrismicBlogPostBodyImagesGallery {
            id
            slice_type
            items {
              caption
              image {
                localFile {
                  childImageSharp {
                    fixed(width: 900, quality: 90) {
                      ...GatsbyImageSharpFixed
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;

export default BlogTemplate;
