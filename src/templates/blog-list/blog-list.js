import React from "react";
import PropTypes from "prop-types";
import { graphql } from "gatsby";
import SEO from "../../components/seo";
import Layout from "../../containers/layout/layout";
import Header from "../../containers/layout/header";
import Footer from "../../containers/layout/footer";
import PageHeader from "../../components/pageheader";
import { Container, Row, Col } from "../../components/ui/wrapper";
import Heading from "../../components/ui/heading";
import Sidebar from "../../containers/blog/sidebar";
import CTA from "../../containers/global/cta-area/section-one";
import Pagination from "../../components/blog/pagination";
import Blog from "../../components/blog/layout-two";
import { BlogListWrapper, BlogBoxWrapper, BlogBox } from "./blog-list.style";

const BlogList = ({ data, pageContext, location, ...restProps }) => {
  const { headingStyle } = restProps;
  const blogs = data.blogContents.edges;

  const { currentPage, numberOfPages } = pageContext;
  return (
    <Layout location={location}>
      <SEO title={`Blog d'Unscuzzy - Page ${currentPage}`} />
      <Header />
      <PageHeader
        pageContext={pageContext}
        location={location}
        title='Blog Update'
      />
      <main className='site-wrapper-reveal'>
        <BlogListWrapper>
          <Container>
            <Row>
              <Col lg={{ span: 4, order: 1 }} xs={{ span: 12, order: 2 }}>
                <Sidebar />
              </Col>
              <Col lg={{ span: 8, order: 2 }} xs={{ span: 12, order: 1 }}>
                <Heading {...headingStyle}>
                  Interesting articles <span>updated daily</span>
                </Heading>
                <BlogBoxWrapper>
                  {blogs.map((blog) => (
                    <BlogBox key={blog.node.uid}>
                      <Blog content={blog.node} />
                    </BlogBox>
                  ))}
                </BlogBoxWrapper>
                <Pagination
                  rootPage='/blog'
                  currentPage={currentPage}
                  numberOfPages={numberOfPages}
                />
              </Col>
            </Row>
          </Container>
        </BlogListWrapper>
        <CTA />
      </main>
      <Footer />
    </Layout>
  );
};

export const query = graphql`
  query BlogListQuery($skip: Int!, $limit: Int!) {
    blogContents: allPrismicBlogPost(
      sort: { fields: data___date, order: DESC }
      limit: $limit
      skip: $skip
    ) {
      edges {
        node {
          data {
            date
            meta_description
            title {
              text
            }
            thumbnail {
              localFile {
                childImageSharp {
                  fluid(maxWidth: 770, maxHeight: 420, quality: 90) {
                    ...GatsbyImageSharpFluid_withWebp
                    presentationHeight
                    presentationWidth
                  }
                }
              }
              alt
            }
          }
          uid
        }
      }
    }
  }
`;

BlogList.propTypes = {
  headingStyle: PropTypes.object,
};

BlogList.defaultProps = {
  headingStyle: {
    as: "h3",
    mb: "70px",
    textalign: "center",
    child: {
      color: "primary",
    },
    responsive: {
      medium: {
        mb: "50px",
      },
    },
  },
};

export default BlogList;
