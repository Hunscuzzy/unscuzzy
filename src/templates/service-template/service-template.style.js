import styled from "styled-components";

import { device } from "../../theme";

export const IntroArea = styled.section`
  padding-top: 100px;
  padding-bottom: 94px;
  @media ${device.medium} {
    padding-top: 72px;
    padding-bottom: 50px;
  }
`;
