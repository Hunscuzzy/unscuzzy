import React from "react";
import PropTypes from "prop-types";
import { graphql } from "gatsby";

import Layout from "../../containers/layout/layout";
import Header from "../../containers/layout/header";
import Footer from "../../containers/layout/footer";
import { Container, Row, Col, Box } from "../../components/ui/wrapper";
import SEO from "../../components/seo";
import PageHeader from "../../components/pageheader";
import Heading from "../../components/ui/heading";
import { IntroArea } from "./service-template.style";
import RecentBlog from "../../components/blog/layout-four";
import {
  BlogList,
  BlogListItem,
} from "../../containers/blog/blog-preview/blog-area.style";
import ContactArea from "../../containers/global/contact-area/contact-three";

const ServiceTemplate = ({
  data: { prismicCategories, allPrismicBlogPost },
  location,
  pageContext,
  ...restProps
}) => {
  const prismicData = prismicCategories.data;
  const {
    introTextStyles: { leftHeading, rightHeading, rightText },
  } = restProps;
  return (
    <Layout location={location}>
      <Header />
      <SEO
        title={prismicData.meta_title}
        description={prismicData.meta_description}
      />
      <PageHeader
        pageContext={pageContext}
        location={location}
        title={`Services ${prismicData.title.text}`}
        image={
          prismicData.thumbnail.localFile &&
          prismicData.thumbnail.localFile.childImageSharp
        }
      />
      <main className='site-wrapper-reveal'>
        <IntroArea>
          <Container>
            <Row>
              <Col lg={4}>
                <Heading {...leftHeading}>
                  À propos de <span> {prismicData.title.text}</span>
                </Heading>
              </Col>
              <Col lg={{ span: 7, offset: 1 }}>
                <Box>
                  {prismicData.subtitle && (
                    <Heading {...rightHeading}>{prismicData.subtitle}</Heading>
                  )}
                  {prismicData.presentation.html && (
                    <div
                      {...rightText}
                      dangerouslySetInnerHTML={{
                        __html: prismicData.presentation.html,
                      }}
                    />
                  )}
                </Box>
              </Col>
            </Row>
          </Container>
        </IntroArea>
        <Container>
          <Row>
            <Col>
              <Heading {...leftHeading} mb='50px'>
                Nos articles de blog <br /> à propos du{" "}
                <span> {prismicData.title.text}</span>
              </Heading>
            </Col>
          </Row>
          <Row mb='100px'>
            <Col sm={12} md={10} lg={6} ml='auto' mr='auto'>
              <BlogList>
                {allPrismicBlogPost &&
                  allPrismicBlogPost.edges.map((recentBlog, i) => (
                    <BlogListItem key={i}>
                      <RecentBlog
                        title={recentBlog.node.data.title.text}
                        slug={recentBlog.node.uid}
                        date={recentBlog.node.data.date}
                        featured_image={
                          recentBlog.node.data.thumbnail.localFile
                            .childImageSharp
                        }
                      />
                    </BlogListItem>
                  ))}
              </BlogList>
            </Col>
          </Row>
        </Container>
        <ContactArea />
      </main>
      <Footer />
    </Layout>
  );
};

export const query = graphql`
  query ServiceBySlug($uid: String!) {
    allPrismicBlogPost(
      filter: {
        data: { relations: { elemMatch: { category: { uid: { eq: $uid } } } } }
      }
    ) {
      edges {
        node {
          data {
            date
            title {
              text
            }
            thumbnail {
              localFile {
                childImageSharp {
                  fluid(maxWidth: 770, maxHeight: 420, quality: 90) {
                    ...GatsbyImageSharpFluid_withWebp
                    presentationHeight
                    presentationWidth
                  }
                }
              }
              alt
            }
          }
          uid
        }
      }
    }
    prismicCategories(uid: { eq: $uid }) {
      data {
        meta_description
        meta_title
        title {
          text
        }
        subtitle
        presentation {
          html
        }
        thumbnail {
          localFile {
            childImageSharp {
              fluid(maxWidth: 1920, maxHeight: 400, quality: 90) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  }
`;
ServiceTemplate.propTypes = {
  taglineStyle: PropTypes.object,
};

ServiceTemplate.defaultProps = {
  titleStyle: {
    as: "h1",
    fontSize: "40px",
    textalign: "center",
    mb: "15px",
    wordwrap: "break-word",
    responsive: {
      medium: {
        fontSize: "35px",
      },
      small: {
        fontSize: "25px",
      },
    },
  },

  introTextStyles: {
    leftHeading: {
      as: "h3",
      mt: "25px",
      fontSize: "38px",
      textalign: "center",
      child: {
        color: "primary",
      },
      responsive: {
        large: {
          fontSize: "30px",
        },
        medium: {
          mt: 0,
          mb: "20px",
          fontSize: "28px",
        },
        small: {
          fontSize: "24px",
        },
      },
    },
    rightHeading: {
      as: "h5",
      position: "relative",
      pl: "34px",
      mb: "20px",
      lineheight: 1.67,
      fontweight: 800,
      layout: "quote",
      child: {
        color: "primary",
      },
    },
    rightText: {
      mt: "15px",
      fontSize: "18px",
      ml: "34px",
      color: "#696969",
    },
  },
};

export default ServiceTemplate;
